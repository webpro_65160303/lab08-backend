import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    return 'This action adds a new product';
  }

  findAll() {
    return this.productsRepository.find({ relations: { type: true }});
  }

  findOne(id: number) {
    return this.productsRepository.findOne({
      where: { id },
      relations: { type: true },
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const updateProduct = await this.productsRepository.findOneByOrFail({
      where: { id },
    });
    await this.productsRepository.update(id, {
      ...updateProduct,
      ...updateProductDto,
    });
    const result = await this.productsRepository.findOne({
      where: { id },
      relations: { type: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.productsRepository.findOneByOrFail({
      where: { id },
    });
    await this.productsRepository.remove(deleteProduct);
    return deleteProduct;
  }
}
