import { Injectable } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Role } from './entities/role.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}
  create(createRoleDto: CreateRoleDto) {
    return 'This action adds a new role';
  }

  findAll() {
    return this.rolesRepository.find();
  }

  findOne(id: number) {
    return this.rolesRepository.findOneBy({ id });
  }

  async update(id: number, updateRoleDto: UpdateRoleDto) {
    await this.rolesRepository.findOneByOrFail({ id });
    await this.rolesRepository.update(id, updateRoleDto);
    const updateType = await this.rolesRepository.findOneBy({ id });
    return updateType;
  }

  async remove(id: number) {
    const removedType = await this.rolesRepository.findOneByOrFail({ id });
    await this.rolesRepository.remove(removedType);
    return removedType;
  }
}
